package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionsEquipment.AttackMissionEquipment.AttackMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;

public class AttackMission extends Mission {

    String target;

    public AttackMission(Coordinates coordinates_of_destination, String pilot_name,
                         AerialVehicle aerial_vehicle, String target) {
        super(coordinates_of_destination, pilot_name, aerial_vehicle);
        this.target = target;
    }

    @Override
    public String executeMission(){
        ArrayList<MissionEquipment> missions_equipment = this.getAerial_vehicle().getMissions_equipment();
        AttackMissionEquipment attack_mission_equipment;
        String attack_mission_equipment_string = "";
        for (MissionEquipment missionEquipment : missions_equipment) {
            attack_mission_equipment = (AttackMissionEquipment) missionEquipment;
            attack_mission_equipment_string += (attack_mission_equipment.getMissile_type() +
                    "X" + attack_mission_equipment.getNum_of_missiles());
        }
        String s = this.getPilot_name() + ": " + this.getAerial_vehicle().getAerial_vehicle_name() +
                " Attacking " + this.target + " " + "with: " + attack_mission_equipment_string;

        return s;
    }

    public void check_mission_capability() throws AerialVehicleNotCompatibleException {
        if(!this.getAerial_vehicle().hasAttackMissionAbility())
        {
            throw new AerialVehicleNotCompatibleException(this.getAerial_vehicle().getAerial_vehicle_name() +
                    ": this aerial vehicle can't perform that mission");
        }
    }
}
