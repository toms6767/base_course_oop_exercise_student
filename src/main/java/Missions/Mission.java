package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission implements MissionOperations {

    private Coordinates coordinates_of_destination;
    private String pilot_name;
    AerialVehicle aerial_vehicle;

    public Mission(Coordinates coordinates_of_destination, String pilot_name,
                   AerialVehicle aerial_vehicle) {
        this.coordinates_of_destination = coordinates_of_destination;
        this.pilot_name = pilot_name;
        this.aerial_vehicle = new AerialVehicle(aerial_vehicle.getAerial_vehicle_name(),
                aerial_vehicle.getRepair_time_value(),
                aerial_vehicle.getFlight_hours_since_last_repair(),
                aerial_vehicle.getBase_location());
        this.aerial_vehicle.setHas_attack_mission_ability(aerial_vehicle.hasAttackMissionAbility());
        this.aerial_vehicle.setHas_intelligence_mission_ability(aerial_vehicle.hasIntelligenceMissionAbility());
        this.aerial_vehicle.setHas_bda_mission_ability(aerial_vehicle.hasBdaMissionAbility());
        this.aerial_vehicle.setMissions_equipment(aerial_vehicle.getMissions_equipment());
    }

    public Coordinates getCoordinates_of_destination() {
        return coordinates_of_destination;
    }

    public String getPilot_name() {
        return pilot_name;
    }

    public AerialVehicle getAerial_vehicle() {
        return aerial_vehicle;
    }

    public void begin() throws AerialVehicleNotCompatibleException {
        check_mission_capability();
        System.out.println("Beginning Mission!");
        this.aerial_vehicle.flyTo(this.coordinates_of_destination);
    }

    public void cancel(){
        System.out.println("Abort Mission!");
        this.aerial_vehicle.land(null);
    }

    public void finish(){
        String s = this.executeMission();
        System.out.println(s);
        this.aerial_vehicle.land(null);
        System.out.println("Finish Mission!");
    }

    public abstract String executeMission();

    public abstract void check_mission_capability() throws AerialVehicleNotCompatibleException;
}
