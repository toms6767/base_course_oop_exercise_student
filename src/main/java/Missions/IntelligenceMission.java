package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment.IntelligenceMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;

public class IntelligenceMission extends Mission {

    String region;

    public IntelligenceMission(Coordinates coordinates_of_destination, String pilot_name,
                               AerialVehicle aerial_vehicle, String region) {
        super(coordinates_of_destination, pilot_name, aerial_vehicle);
        this.region = region;
    }

    @Override
    public String executeMission(){
        ArrayList<MissionEquipment> missions_equipment = this.getAerial_vehicle().getMissions_equipment();
        IntelligenceMissionEquipment intelligence_mission_equipment;
        String intelligence_mission_equipment_string = "";
        for (MissionEquipment missionEquipment : missions_equipment) {
            intelligence_mission_equipment = (IntelligenceMissionEquipment) missionEquipment;
            intelligence_mission_equipment_string += (intelligence_mission_equipment.getSensor_type());
        }
        String s = this.getPilot_name() + ": " + this.getAerial_vehicle().getAerial_vehicle_name() +
                " Collecting Data in " + this.region + " " + "with: " + intelligence_mission_equipment_string;

        return s;
    }

    public void check_mission_capability() throws AerialVehicleNotCompatibleException {
        if(!this.getAerial_vehicle().hasIntelligenceMissionAbility())
        {
            throw new AerialVehicleNotCompatibleException(this.getAerial_vehicle().getAerial_vehicle_name() +
                    ": this aerial vehicle can't perform that mission");
        }
    }
}
