package Missions;

import Missions.AerialVehicleNotCompatibleException;

public interface MissionOperations {
    void begin() throws AerialVehicleNotCompatibleException;
    void cancel();
    void finish();
}
