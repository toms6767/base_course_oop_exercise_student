package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionsEquipment.BdaMissionEquipment.BdaMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;

public class BdaMission extends Mission {

    String objective;

    public BdaMission(Coordinates coordinates_of_destination, String pilot_name,
                      AerialVehicle aerial_vehicle, String objective) {
        super(coordinates_of_destination, pilot_name, aerial_vehicle);
        this.objective = objective;
    }

    @Override
    public String executeMission(){
        ArrayList<MissionEquipment> missions_equipment = this.getAerial_vehicle().getMissions_equipment();
        BdaMissionEquipment bda_mission_equipment;
        String bda_mission_equipment_string = "";
        for (MissionEquipment missionEquipment : missions_equipment) {
            bda_mission_equipment = (BdaMissionEquipment) missionEquipment;
            bda_mission_equipment_string += (bda_mission_equipment.getCamera_type());
        }
        String s = this.getPilot_name() + ": " + this.getAerial_vehicle().getAerial_vehicle_name() +
                " taking pictures of " + this.objective + " " + "with: " + bda_mission_equipment_string;

        return s;
    }

    public void check_mission_capability() throws AerialVehicleNotCompatibleException {
        if(!this.getAerial_vehicle().hasBdaMissionAbility())
        {
            throw new AerialVehicleNotCompatibleException(this.getAerial_vehicle().getAerial_vehicle_name() +
                    ": this aerial vehicle can't perform that mission");
        }
    }
}
