package AerialVehicles;

import Entities.Coordinates;

public interface FlightOperations {
    void flyTo(Coordinates destination);
    void land(Coordinates destination);
}
