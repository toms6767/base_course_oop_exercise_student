package AerialVehicles.Katmams;

import Entities.Coordinates;

public interface KatmamOperations {
    String hoverOverLocation(Coordinates destination);
}
