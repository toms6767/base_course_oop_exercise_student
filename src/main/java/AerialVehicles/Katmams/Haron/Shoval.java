package AerialVehicles.Katmams.Haron;

import AerialVehicles.MissionsEquipment.AttackMissionEquipment.AttackMissionEquipment;
import AerialVehicles.MissionsEquipment.BdaMissionEquipment.BdaMissionEquipment;
import AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment.IntelligenceMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;


public class Shoval extends HaronKatmam {

    ArrayList<MissionEquipment> missions_equipment = new ArrayList<>();

    public Shoval(Coordinates base_location) {
        super("Shoval", base_location);
        this.setHas_attack_mission_ability(true);
        this.setHas_intelligence_mission_ability(true);
        this.setHas_bda_mission_ability(true);
    }

    public void addAttackMissionEquipment(AttackMissionEquipment attack_mission_equipment) {
        MissionEquipment new_attack_mission_fields = new AttackMissionEquipment(
                attack_mission_equipment.getNum_of_missiles(), attack_mission_equipment.getMissile_type());
        this.getMissions_equipment().add(new_attack_mission_fields);
    }

    public void addIntelligenceMissionEquipment(IntelligenceMissionEquipment intelligence_mission_equipment) {
        MissionEquipment new_intelligence_mission_fields = new IntelligenceMissionEquipment(
                intelligence_mission_equipment.getSensor_type());
        this.getMissions_equipment().add(new_intelligence_mission_fields);
    }

    public void addBdaMissionEquipment(BdaMissionEquipment bda_mission_equipment) {
        MissionEquipment new_bda_mission_fields = new BdaMissionEquipment(
                bda_mission_equipment.getCamera_type());
        this.getMissions_equipment().add(new_bda_mission_fields);
    }
}

