package AerialVehicles.Katmams.Haron;

import AerialVehicles.Katmams.Katmam;
import Entities.Coordinates;

public class HaronKatmam extends Katmam {

    private static final int REPAIR_TIME_VALUE = 150;

    public HaronKatmam(String aerial_vehicle_name, Coordinates base_location) {
        super(aerial_vehicle_name, REPAIR_TIME_VALUE, base_location);
    }
}
