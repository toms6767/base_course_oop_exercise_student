package AerialVehicles.Katmams.Hermes;

import AerialVehicles.MissionsEquipment.BdaMissionEquipment.BdaMissionEquipment;
import AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment.IntelligenceMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;


public class Zik extends HermesKatmam {

    ArrayList<MissionEquipment> missions_equipment = new ArrayList<>();

    public Zik(Coordinates base_location) {
        super("Zik", base_location);
        this.setHas_intelligence_mission_ability(true);
        this.setHas_bda_mission_ability(true);
    }

    public void addIntelligenceMissionEquipment(IntelligenceMissionEquipment intelligence_mission_equipment) {
        MissionEquipment new_intelligence_mission_fields = new IntelligenceMissionEquipment(
                intelligence_mission_equipment.getSensor_type());
        this.getMissions_equipment().add(new_intelligence_mission_fields);
    }

    public void addBdaMissionEquipment(BdaMissionEquipment bda_mission_equipment) {
        MissionEquipment new_bda_mission_fields = new BdaMissionEquipment(
                bda_mission_equipment.getCamera_type());
        this.getMissions_equipment().add(new_bda_mission_fields);
    }
}