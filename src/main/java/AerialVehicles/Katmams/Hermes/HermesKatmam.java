package AerialVehicles.Katmams.Hermes;

import AerialVehicles.Katmams.Katmam;
import Entities.Coordinates;

public class HermesKatmam extends Katmam {

    private static final int REPAIR_TIME_VALUE = 100;

    public HermesKatmam(String aerial_vehicle_name, Coordinates base_location) {
        super(aerial_vehicle_name, REPAIR_TIME_VALUE, base_location);
    }
}
