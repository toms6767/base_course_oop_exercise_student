package AerialVehicles.Katmams;

import AerialVehicles.AerialVehicle;
import AerialVehicles.FlightStatusCode;
import Entities.Coordinates;

public class Katmam extends AerialVehicle implements KatmamOperations {

    public Katmam(String aerial_vehicle_name, int repair_time_value, Coordinates base_location) {
        super(aerial_vehicle_name, repair_time_value, 0, base_location);
    }

    public String hoverOverLocation(Coordinates destination){
        this.setFlight_status(FlightStatusCode.IN_THE_AIR);
        String s = "Hovering Over: " + destination;
        System.out.println(s);
        return s;
    }
}
