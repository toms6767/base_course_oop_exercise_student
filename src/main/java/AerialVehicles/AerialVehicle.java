package AerialVehicles;

import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;

public class AerialVehicle implements FlightOperations, MaintenanceOperations{

    private String aerial_vehicle_name;
    private final int repair_time_value;
    private int flight_hours_since_last_repair;
    private FlightStatusCode flight_status;
    private final Coordinates base_location;
    private boolean has_attack_mission_ability;
    private boolean has_intelligence_mission_ability;
    private boolean has_bda_mission_ability;
    ArrayList<MissionEquipment> missions_equipment = new ArrayList<>();

    public AerialVehicle(String aerial_vehicle_name, int repair_time_value, int flight_hours_since_last_repair, Coordinates base_location) {
        this.aerial_vehicle_name = aerial_vehicle_name;
        this.repair_time_value = repair_time_value;
        this.flight_hours_since_last_repair = flight_hours_since_last_repair;
        this.flight_status = FlightStatusCode.READY_TO_FLY;
        this.base_location = base_location;
        this.has_attack_mission_ability = false;
        this.has_intelligence_mission_ability = false;
        this.has_bda_mission_ability = false;

    }

    public String getAerial_vehicle_name() {
        return aerial_vehicle_name;
    }

    public FlightStatusCode getFlight_status() {
        return flight_status;
    }

    public void setFlight_status(FlightStatusCode flight_status) {
        this.flight_status = flight_status;
    }

    public int getRepair_time_value() {
        return repair_time_value;
    }

    public int getFlight_hours_since_last_repair() {
        return flight_hours_since_last_repair;
    }

    public void setFlight_hours_since_last_repair(int flight_hours_since_last_repair) {
        this.flight_hours_since_last_repair = flight_hours_since_last_repair;
    }

    public Coordinates getBase_location() {
        return base_location;
    }

    public void setHas_attack_mission_ability(boolean has_attack_mission_ability) {
        this.has_attack_mission_ability = has_attack_mission_ability;
    }

    public void setHas_intelligence_mission_ability(boolean has_intelligence_mission_ability) {
        this.has_intelligence_mission_ability = has_intelligence_mission_ability;
    }

    public void setHas_bda_mission_ability(boolean has_bda_mission_ability) {
        this.has_bda_mission_ability = has_bda_mission_ability;
    }

    public boolean hasAttackMissionAbility() {
        return has_attack_mission_ability;
    }

    public boolean hasIntelligenceMissionAbility() {
        return has_intelligence_mission_ability;
    }

    public boolean hasBdaMissionAbility() {
        return has_bda_mission_ability;
    }

    public void flyTo(Coordinates destination){
        if(this.getFlight_status() == FlightStatusCode.READY_TO_FLY)
        {
            System.out.println("Flying to: " + destination.get_coordinates_in_string());
            this.setFlight_status(FlightStatusCode.IN_THE_AIR);
        }
        else
        {
            System.out.println("FAerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination){
        if(destination != null)
        {
            System.out.println("Landing on: " + destination.get_coordinates_in_string());
        }
        else
        {
            System.out.println("Landing on: " + base_location.get_coordinates_in_string());
        }
        this.check();
    }

    public void check(){
        if(this.getFlight_hours_since_last_repair() >= this.getRepair_time_value())
        {
            this.setFlight_status(FlightStatusCode.NOT_READY_TO_FLY);
            this.repair();
        }
        else
        {
            this.setFlight_status(FlightStatusCode.READY_TO_FLY);
        }
    }

    public void repair(){
        this.setFlight_hours_since_last_repair(0);
        this.setFlight_status(FlightStatusCode.READY_TO_FLY);
    }

    public ArrayList<MissionEquipment> getMissions_equipment() {
        return missions_equipment;
    }

    public void setMissions_equipment(ArrayList<MissionEquipment> missions_equipment) {
        this.missions_equipment = missions_equipment;
    }
}
