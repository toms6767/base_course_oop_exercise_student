package AerialVehicles.FighterJets;

import AerialVehicles.MissionsEquipment.AttackMissionEquipment.AttackMissionEquipment;
import AerialVehicles.MissionsEquipment.BdaMissionEquipment.BdaMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;

import java.util.ArrayList;


public class F16 extends Plane {

    ArrayList<MissionEquipment> missions_equipment = new ArrayList<>();

    public F16(Coordinates base_location) {
        super("F16", base_location);
        this.setHas_attack_mission_ability(true);
        this.setHas_bda_mission_ability(true);
    }

    public void addAttackMissionEquipment(AttackMissionEquipment attack_mission_equipment) {
        MissionEquipment new_attack_mission_fields = new AttackMissionEquipment(
                attack_mission_equipment.getNum_of_missiles(), attack_mission_equipment.getMissile_type());
        this.getMissions_equipment().add(new_attack_mission_fields);
    }

    public void addBdaMissionEquipment(BdaMissionEquipment bda_mission_equipment) {
        MissionEquipment new_bda_mission_fields = new BdaMissionEquipment(
                bda_mission_equipment.getCamera_type());
        this.getMissions_equipment().add(new_bda_mission_fields);
    }
}
