package AerialVehicles.FighterJets;

import AerialVehicles.MissionsEquipment.AttackMissionEquipment.AttackMissionEquipment;
import AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment.IntelligenceMissionEquipment;
import AerialVehicles.MissionsEquipment.MissionEquipment;
import Entities.Coordinates;


public class F15 extends Plane {

    public F15(Coordinates base_location) {
        super("F15", base_location);
        this.setHas_attack_mission_ability(true);
        this.setHas_intelligence_mission_ability(true);
    }

    public void addAttackMissionEquipment(AttackMissionEquipment attack_mission_equipment) {
        MissionEquipment new_attack_mission_fields = new AttackMissionEquipment(
                attack_mission_equipment.getNum_of_missiles(), attack_mission_equipment.getMissile_type());
        this.getMissions_equipment().add(new_attack_mission_fields);
    }

    public void addIntelligenceMissionEquipment(IntelligenceMissionEquipment intelligence_mission_equipment) {
        MissionEquipment new_intelligence_mission_fields = new IntelligenceMissionEquipment(
                intelligence_mission_equipment.getSensor_type());
        this.getMissions_equipment().add(new_intelligence_mission_fields);
    }
}
