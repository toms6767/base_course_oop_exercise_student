package AerialVehicles.FighterJets;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class Plane extends AerialVehicle {

    private static final int REPAIR_TIME_VALUE = 250;

    public Plane(String aerial_vehicle_name, Coordinates base_location) {
        super(aerial_vehicle_name, REPAIR_TIME_VALUE, 0, base_location);
    }
}
