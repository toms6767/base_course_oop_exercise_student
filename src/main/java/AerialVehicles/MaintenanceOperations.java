package AerialVehicles;


public interface MaintenanceOperations {
    void check();
    void repair();
}
