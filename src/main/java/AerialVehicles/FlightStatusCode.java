package AerialVehicles;

public enum FlightStatusCode {
    READY_TO_FLY,
    NOT_READY_TO_FLY,
    IN_THE_AIR
}
