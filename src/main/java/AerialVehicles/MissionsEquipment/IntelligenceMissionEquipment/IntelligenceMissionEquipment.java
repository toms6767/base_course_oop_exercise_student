package AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment;

import AerialVehicles.MissionsEquipment.MissionEquipment;

public class IntelligenceMissionEquipment extends MissionEquipment {

    private final SensorType sensor_type;

    public IntelligenceMissionEquipment(SensorType sensor_type) {
        this.sensor_type = sensor_type;
    }

    public SensorType getSensor_type() {
        return sensor_type;
    }
}
