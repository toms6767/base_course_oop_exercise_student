package AerialVehicles.MissionsEquipment.BdaMissionEquipment;

public enum CameraType {
    Regular,
    Thermal,
    NightVision
}
