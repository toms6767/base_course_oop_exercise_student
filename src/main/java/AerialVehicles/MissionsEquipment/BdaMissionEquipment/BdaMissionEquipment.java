package AerialVehicles.MissionsEquipment.BdaMissionEquipment;

import AerialVehicles.MissionsEquipment.MissionEquipment;

public class BdaMissionEquipment extends MissionEquipment {

    private final CameraType camera_type;

    public BdaMissionEquipment(CameraType camera_type) {
        this.camera_type = camera_type;
    }

    public CameraType getCamera_type() {
        return camera_type;
    }
}
