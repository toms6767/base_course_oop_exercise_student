package AerialVehicles.MissionsEquipment.AttackMissionEquipment;

import AerialVehicles.MissionsEquipment.MissionEquipment;

public class AttackMissionEquipment extends MissionEquipment {

    private final int num_of_missiles;
    private final MissileType missile_type;

    public AttackMissionEquipment(int num_of_missiles, MissileType missile_type) {
        this.num_of_missiles = num_of_missiles;
        this.missile_type = missile_type;
    }

    public int getNum_of_missiles() {
        return num_of_missiles;
    }

    public MissileType getMissile_type() {
        return missile_type;
    }
}
