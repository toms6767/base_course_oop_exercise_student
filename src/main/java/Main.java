import AerialVehicles.FighterJets.F15;
import AerialVehicles.FighterJets.F16;
import AerialVehicles.Katmams.Haron.Eitan;
import AerialVehicles.Katmams.Haron.Shoval;
import AerialVehicles.Katmams.Hermes.Kochav;
import AerialVehicles.Katmams.Hermes.Zik;
import AerialVehicles.MissionsEquipment.AttackMissionEquipment.AttackMissionEquipment;
import AerialVehicles.MissionsEquipment.AttackMissionEquipment.MissileType;
import AerialVehicles.MissionsEquipment.BdaMissionEquipment.BdaMissionEquipment;
import AerialVehicles.MissionsEquipment.BdaMissionEquipment.CameraType;
import AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment.IntelligenceMissionEquipment;
import AerialVehicles.MissionsEquipment.IntelligenceMissionEquipment.SensorType;
import Entities.Coordinates;
import Missions.*;

public class Main {
    public static void main(String[] args) {

        //=========================================================================

        Coordinates f15_coordinates = new Coordinates(10.0, 10.0);
        F15 f15 = new F15(f15_coordinates);

        Coordinates f16_coordinates = new Coordinates(10.5, 20.0);
        F16 f16 = new F16(f16_coordinates);

        Coordinates eitan_coordinates = new Coordinates(12.5, 27.7);
        Eitan eitan = new Eitan(eitan_coordinates);

        Coordinates kochav_coordinates = new Coordinates(100.8, 20.0);
        Kochav kochav = new Kochav(kochav_coordinates);

        Coordinates shoval_coordinates = new Coordinates(100.8, 20.0);
        Shoval shoval = new Shoval(shoval_coordinates);

        Coordinates zik_coordinates = new Coordinates(150.1, 50.9);
        Zik zik = new Zik(zik_coordinates);

        //=========================================================================

        Coordinates attack_mission_dest = new Coordinates(33.6, 200.7);
        AttackMissionEquipment attack_mission_equipment = new AttackMissionEquipment(10, MissileType.Amram);
        f15.addAttackMissionEquipment(attack_mission_equipment);
        Mission attack_mission = new AttackMission(attack_mission_dest, "tom shapira", f15, "attack_mission");
        try {
            attack_mission.begin();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
            return;
        }
        attack_mission.finish();

        //=========================================================================

        Coordinates bda_mission_dest = new Coordinates(37.5, 210.8);
        BdaMissionEquipment bda_mission_equipment = new BdaMissionEquipment(CameraType.NightVision);
        shoval.addBdaMissionEquipment(bda_mission_equipment);
        Mission bda_mission = new BdaMission(bda_mission_dest, "tom shapira", shoval, "bda_mission");
        try {
            bda_mission.begin();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
            return;
        }
        bda_mission.finish();

        //=========================================================================

        Coordinates intelligence_mission_dest = new Coordinates(37.5, 210.8);
        IntelligenceMissionEquipment intelligence_mission_equipment = new IntelligenceMissionEquipment(SensorType.Elint);
        eitan.addIntelligenceMissionEquipment(intelligence_mission_equipment);
        Mission intelligence_mission = new IntelligenceMission(intelligence_mission_dest, "tom shapira", eitan, "intelligence_mission");
        try {
            intelligence_mission.begin();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
            return;
        }
        intelligence_mission.finish();

        //=========================================================================

        //example of aerial vehicle can't perform that mission
        Coordinates attack_mission_dest_2 = new Coordinates(33.6, 200.7);
        BdaMissionEquipment bda_mission_equipment_2 = new BdaMissionEquipment(CameraType.NightVision);
        zik.addBdaMissionEquipment(bda_mission_equipment_2);
        Mission attack_mission_2 = new AttackMission(attack_mission_dest, "tom shapira", zik, "attack_mission");
        try {
            attack_mission_2.begin();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
            return;
        }
        attack_mission_2.finish();

        //=========================================================================




    }
}
